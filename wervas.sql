-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for wervas
CREATE DATABASE IF NOT EXISTS `wervas` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `wervas`;

-- Dumping structure for table wervas.admin_login
CREATE TABLE IF NOT EXISTS `admin_login` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.admin_login: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_login` DISABLE KEYS */;
INSERT INTO `admin_login` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'wervas_admin', '$2y$10$aNeTAAFSsIH/28kIqlv1yOjSYldXxIXJeVz3khnrKyM.91lzJqRoS', '2020-09-21 11:36:59', '2020-09-21 11:36:59');
/*!40000 ALTER TABLE `admin_login` ENABLE KEYS */;

-- Dumping structure for table wervas.admin_reg
CREATE TABLE IF NOT EXISTS `admin_reg` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` bigint(20) unsigned NOT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.admin_reg: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_reg` DISABLE KEYS */;
INSERT INTO `admin_reg` (`id`, `name`, `email`, `phone_no`, `username`, `created_at`, `updated_at`) VALUES
	(1, 'WERVAS', 'wervas@gmail.com', 1234569875, 'wervas_admin', '2020-09-21 11:36:59', '2020-09-21 11:36:59');
/*!40000 ALTER TABLE `admin_reg` ENABLE KEYS */;

-- Dumping structure for table wervas.associates
CREATE TABLE IF NOT EXISTS `associates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `doj` varchar(100) NOT NULL,
  `account_status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table wervas.associates: ~0 rows (approximately)
/*!40000 ALTER TABLE `associates` DISABLE KEYS */;
INSERT INTO `associates` (`id`, `username`, `fname`, `lname`, `address`, `contact_number`, `email`, `doj`, `account_status`, `created_at`, `updated_at`) VALUES
	(1, 'jack101', 'Jack', 'Smith', 'Puri, Orissa', '9513578520', 'msayandeep1993@gmail.com', '2021-02-11', 1, '2021-02-10 06:09:39', '2021-02-10 06:09:39');
/*!40000 ALTER TABLE `associates` ENABLE KEYS */;

-- Dumping structure for table wervas.associate_login
CREATE TABLE IF NOT EXISTS `associate_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table wervas.associate_login: ~0 rows (approximately)
/*!40000 ALTER TABLE `associate_login` DISABLE KEYS */;
INSERT INTO `associate_login` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'jack101', '$2y$10$dIJua74APPUmzyw7aeh/WOOtrDB8WnxuSRaiZkjDL6wjzbPItmCxe', '2021-02-10 06:09:39', '2021-02-10 06:09:39');
/*!40000 ALTER TABLE `associate_login` ENABLE KEYS */;

-- Dumping structure for table wervas.contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` (`id`, `name`, `phone`, `relation`, `email`, `address`, `created_at`, `updated_at`) VALUES
	(1, 'Vendor', '6548523255', 'brother', 'sayandeepmajumdar.official@gmail.com', 'pola hu', '2021-02-09 22:38:38', '2021-02-09 22:39:12');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;

-- Dumping structure for table wervas.customer_account
CREATE TABLE IF NOT EXISTS `customer_account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salutation` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondary_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_payment_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_payment_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_plan` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_plan` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.customer_account: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_account` DISABLE KEYS */;
INSERT INTO `customer_account` (`id`, `username`, `salutation`, `fname`, `lname`, `contact_number`, `dob`, `business_phone`, `email`, `secondary_email`, `website`, `country`, `last_payment_date`, `next_payment_date`, `current_plan`, `next_plan`, `street_address`, `skype_id`, `city`, `state`, `zip`, `shipping_address`, `account_status`, `created_at`, `updated_at`) VALUES
	(1, 'jackadmin', 'Mr.', 'Sayandeep', 'Majumdar', '8240131911', NULL, NULL, 'smajumdar1993@gmail.com', NULL, NULL, NULL, NULL, '2021-09-01', '1', NULL, NULL, NULL, NULL, NULL, NULL, 'One Silver', 1, '2021-02-09 22:14:31', '2021-02-09 22:16:12');
/*!40000 ALTER TABLE `customer_account` ENABLE KEYS */;

-- Dumping structure for table wervas.customer_login
CREATE TABLE IF NOT EXISTS `customer_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_time_login` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table wervas.customer_login: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_login` DISABLE KEYS */;
INSERT INTO `customer_login` (`id`, `username`, `password`, `first_time_login`, `created_at`, `updated_at`) VALUES
	(1, 'jackadmin', '$2y$10$1TpBjPwY.sYukI201tFZSuyksgqfVlca3m8pAGlND9J0ZkXgzPUFy', 1, '2021-02-09 22:16:12', '2021-02-09 22:16:12');
/*!40000 ALTER TABLE `customer_login` ENABLE KEYS */;

-- Dumping structure for table wervas.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table wervas.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.migrations: ~11 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(6, '2014_10_12_000000_create_users_table', 1),
	(7, '2014_10_12_100000_create_password_resets_table', 1),
	(8, '2019_08_19_000000_create_failed_jobs_table', 1),
	(9, '2020_09_21_051658_create_admin_reg_table', 2),
	(10, '2020_09_21_052153_create_admin_login_table', 2),
	(11, '2020_09_21_175848_create_plan_packages_table', 3),
	(12, '2020_10_05_125835_create_customer_account_table', 4),
	(13, '2020_10_05_131556_create_customer_lead_table', 5),
	(14, '2021_01_19_070012_create_contacts_table', 6),
	(15, '2021_01_23_145955_create_web_logins_table', 7),
	(16, '2021_02_11_063849_create_task_attachments_table', 8),
	(17, '2021_03_06_080802_create_task_comments_table', 9);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table wervas.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table wervas.plan_packages
CREATE TABLE IF NOT EXISTS `plan_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validity_in_days` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_amount` int(11) NOT NULL,
  `total_point_charged` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.plan_packages: ~2 rows (approximately)
/*!40000 ALTER TABLE `plan_packages` DISABLE KEYS */;
INSERT INTO `plan_packages` (`id`, `plan_name`, `description`, `validity_in_days`, `paypal_url`, `package_amount`, `total_point_charged`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Silver', 'One for All', '365', 'http://localhost:8000/', 150, '', 1, '2021-02-09 22:13:04', '2021-02-09 22:13:04'),
	(2, 'Golden', 'Enter jkaskjas', '120', 'http://localhost/phpmyadmin/index.php?db=asset_comply&table=users&target=sql.php', 4522, '1200', 1, '2021-02-11 08:01:24', '2021-02-11 08:01:24');
/*!40000 ALTER TABLE `plan_packages` ENABLE KEYS */;

-- Dumping structure for table wervas.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_no` varchar(100) NOT NULL,
  `task_type` varchar(100) DEFAULT NULL,
  `customer_email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `comments` varchar(400) DEFAULT NULL,
  `resolutions` varchar(300) DEFAULT NULL,
  `task_priority` varchar(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `assigned_associate` varchar(100) DEFAULT NULL,
  `last_modified` varchar(100) DEFAULT NULL,
  `task_status` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table wervas.tasks: ~7 rows (approximately)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`id`, `task_no`, `task_type`, `customer_email`, `subject`, `description`, `comments`, `resolutions`, `task_priority`, `created_by`, `assigned_associate`, `last_modified`, `task_status`, `created_at`, `updated_at`) VALUES
	(1, '121', 'General', 'smajumdar1993@gmail.com', 'weekly report', 'ansansas', 'something', 'something', 'high', 'jackadmin', NULL, NULL, '1', '2021-02-10 06:44:17', '2021-02-10 06:44:17'),
	(2, '10231', 'General', 'sayandeep.tal@gmail.com', 'Changes in my domain (Edited)', 'jnsjknskdskdnksjdnksdsd', 'smmmsdmmsdm', 'something', 'low', 'jackadmin', NULL, NULL, '1', '2021-02-10 07:34:10', '2021-02-10 07:34:10'),
	(3, '1023132', 'General', 'sayandeep.tal@gmail.com', 'Changes in my domain (Edited)', 'jnsjknskdskdnksjdnksdsd', 'smmmsdmmsdm', 'something', 'low', 'jackadmin', NULL, NULL, '1', '2021-02-10 07:35:09', '2021-02-10 07:35:09'),
	(4, '102300', 'General', 'sayandeep.tal@gmail.com', 'Changes in my domain (Edited)', 'jnsjknskdskdnksjdnksdsd', 'smmmsdmmsdm', 'something', 'low', 'jackadmin', NULL, NULL, '1', '2021-02-10 07:35:56', '2021-02-10 07:35:56'),
	(5, '11111', 'General', 'smajumdar1993@gmail.com', 'Changes in my domain (Edited)', 'jkn', 'jn', NULL, 'high', 'jackadmin', NULL, NULL, '1', '2021-02-10 08:46:19', '2021-02-10 08:46:19'),
	(10, '2021Feb2089', 'General', 'volitionllp@gmail.com', 'Changes in my domain (Edited)', 'NA', 'NA', NULL, 'high', 'jackadmin', 'jack101', '2021-02-11 07:54:00', '999', '2021-02-11 07:07:05', '2021-02-11 07:54:01'),
	(11, '2021Feb8451', 'General', 'smajumdar1993@gmail.com', 'Changes in my domain (Edited)', 'nsns', 'nnmnnm', NULL, 'high', 'jackadmin', 'jack101', '2021-02-11 07:49:22', '999', '2021-02-11 07:09:02', '2021-02-11 07:49:23'),
	(12, '2021Feb7477', NULL, 'msayandeep1993@gmail.com', 'Domain Transfer', 'kanskjnksandkjnsad as jd nk', NULL, NULL, 'Normal', 'jackadmin', NULL, NULL, '1', '2021-02-23 07:01:05', '2021-02-23 07:01:05');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;

-- Dumping structure for table wervas.task_assign_transactions
CREATE TABLE IF NOT EXISTS `task_assign_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_no` varchar(100) NOT NULL,
  `associate_uname` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table wervas.task_assign_transactions: ~2 rows (approximately)
/*!40000 ALTER TABLE `task_assign_transactions` DISABLE KEYS */;
INSERT INTO `task_assign_transactions` (`id`, `task_no`, `associate_uname`, `created_at`, `updated_at`) VALUES
	(1, '2021Feb8451', 'jack101', '2021-02-11 07:49:23', '2021-02-11 07:49:23'),
	(2, '2021Feb2089', 'jack101', '2021-02-11 07:54:01', '2021-02-11 07:54:01');
/*!40000 ALTER TABLE `task_assign_transactions` ENABLE KEYS */;

-- Dumping structure for table wervas.task_attachments
CREATE TABLE IF NOT EXISTS `task_attachments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `task_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_attachments` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.task_attachments: ~4 rows (approximately)
/*!40000 ALTER TABLE `task_attachments` DISABLE KEYS */;
INSERT INTO `task_attachments` (`id`, `task_no`, `customer_email`, `task_attachments`, `created_at`, `updated_at`) VALUES
	(14, '2021Feb2089', 'volitionllp@gmail.com', 'filmora-x-survey-icon.jpg', '2021-02-11 07:07:05', '2021-02-11 07:07:05'),
	(15, '2021Feb2089', 'volitionllp@gmail.com', 'FM9-logo.png', '2021-02-11 07:07:05', '2021-02-11 07:07:05'),
	(16, '2021Feb2089', 'volitionllp@gmail.com', 'free-filmstocks-icon.jpg', '2021-02-11 07:07:05', '2021-02-11 07:07:05'),
	(17, '2021Feb8451', 'smajumdar1993@gmail.com', '61c6c8f7df1818e3a603e9e44d315a90.jpg', '2021-02-11 07:09:02', '2021-02-11 07:09:02');
/*!40000 ALTER TABLE `task_attachments` ENABLE KEYS */;

-- Dumping structure for table wervas.task_comments
CREATE TABLE IF NOT EXISTS `task_comments` (
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `task_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_from` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.task_comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `task_comments` DISABLE KEYS */;
INSERT INTO `task_comments` (`uuid`, `task_no`, `comments`, `reply_from`, `timestamp`, `created_at`, `updated_at`) VALUES
	('368e9a40-7e2e-11eb-96c5-efa1bdc81f5e', '121', 'this is insane', 'jackadmin', '2021-03-06 03:44:11', '2021-03-06 03:44:11', '2021-03-06 03:44:11');
/*!40000 ALTER TABLE `task_comments` ENABLE KEYS */;

-- Dumping structure for table wervas.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table wervas.web_logins
CREATE TABLE IF NOT EXISTS `web_logins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table wervas.web_logins: ~0 rows (approximately)
/*!40000 ALTER TABLE `web_logins` DISABLE KEYS */;
INSERT INTO `web_logins` (`id`, `username`, `web_url`, `password`, `added_by`, `created_at`, `updated_at`) VALUES
	(1, 'msayandeep1993@gmail.com', 'http://localhost/phpmyadmin/tbl_structure.php', '5454545454544', 'jackadmin', '2021-02-09 22:39:39', '2021-02-09 22:39:39');
/*!40000 ALTER TABLE `web_logins` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
