<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['middleware' => ''],function(){   //Session validation in every pages

Route::prefix('admin')->group(function () {

    Route::get('/','AdminAuthController@signInView');

    Route::post('/auth/service','AdminAuthController@signInService')
    			->name('auth.admin');
    Route::get('/dashboard','AdminAuthController@dashboardView')
    			->name('admin.dashboard');

    // Adding Plan Packages

    Route::get('/packages','PackagesController@addPackagesView')
    		    ->name('admin.packages.view');

    Route::post('/packages/save','PackagesController@savePackages')
    			->name('admin.packages.save');

    Route::get('/packages/list','PackagesController@listOfPackages')
    			->name('admin.packages.list');

    Route::get('/packages/status/{status}/{id}','PackagesController@changePackageStatus')
    			->name('admin.packages.status');

    Route::post('/packages/update','PackagesController@updatePackages')
    			->name('admin.packages.update');


    Route::get('/customers/list','CustomerController@customerListInAdmin')
                ->name('admin.customers.list');
    Route::post('/customers/accept','CustomerController@customerAcceptance')
                ->name('admin.customer.accept');

    Route::get('/customers/profile/{username}','CustomerController@singleCustomerProfile')
                ->name('admin.customer.profile');

    Route::get('/customers/profile/status/{username}/{status}','CustomerController@singleCustomerProfileStatusUpdate')
                ->name('admin.customer.profile.status');

    Route::get('/customers/account/create','CustomerController@createCustomerAccount')
                ->name('admin.customer.create');
    
    Route::post('/customers/account/save','CustomerController@saveCustomerAccountInfo')
                ->name('admin.customer.save');

    Route::get('/associate','AssociateController@addAssociateView')
                ->name('admin.view.associate');

    Route::post('/associate/add', 'AssociateController@saveAssociate')
                ->name('admin.associate.save');

    Route::get('/associate/list','AssociateController@listOfAssociates')
                ->name('admin.associate.list');
    Route::post('/associate/status','AssociateController@updateAssociateAccountStatus')
                ->name('admin.associate.status.update');

    Route::get('/tasks','TaskController@listOfTasksForAdmin')
    			->name('admin.tasks.view');

    Route::get('/tasks/info/{id}','TaskController@taskInfoForAdmin')
    		    ->name('admin.task.info');
    Route::post('/tasks/assigning/associate','AssociateController@assignTaskToAssociate')
    			->name('admin.assign.associate');

});


// });
 
//  Admin Route Ends
 
// Associate Routes 

Route::get('/associate/','AssociateAuthController@signInView')
                            ->name('assoc.login.view');
Route::get('/associate/login','AssociateAuthController@signInView');

Route::post('/assoc/auth/service','AssociateAuthController@signInService')
                ->name('assoc.auth');


// Route::group(['middleware' => ''],function(){   //Session validation in every pages

Route::prefix('/wervas/associates')->group(function () {

    Route::get('/home','AssociateAuthController@dashboardView')
                ->name('assoc.dashboard');

    Route::get('/tasks','TaskController@getListOfTasksForAssoc')
                ->name('assoc.tasks');

    Route::get('/tasks/info/{id}','TaskController@taskInfoForAssociates')
                ->name('assoc.task.info');

   
});


// });

//  Associate Routes End



// Customer Routes 

Route::get('/','CustomerAuthController@signUpView');
Route::get('/login','CustomerAuthController@signInView');

Route::post('/customers/auth/service','CustomerAuthController@signInService')
                ->name('customer.auth');

Route::post('/customer/signup/auth','CustomerAuthController@signUpService')
                ->name('customer.register');

// Route::group(['middleware' => ''],function(){   //Session validation in every pages

Route::prefix('customer')->group(function () {

    Route::get('/dashboard','CustomerController@dashboardView')
                ->name('customer.dashboard');
   
    Route::get('/task','TaskController@taskViewPage')
    		->name('customer.task.view');
    
    Route::post('/task/create','TaskController@taskCreate')
    		->name('customer.task.create');

    Route::get('/task/list','TaskController@tasksList')
    		->name('customer.task.list');
    
    Route::get('/tasks/info/{id}','TaskController@taskInfoForCustomer')
                ->name('customer.task.info');

    Route::get('attachment/{filename}', 'TaskController@displayAttachment')
    			->name('attachment.display');

    Route::post('/task/update','TaskController@updateTask')
    			->name('customer.task.update');
    			
    Route::post('/task/delete','TaskController@deleteTask')
    			->name('customer.task.delete');
    
    Route::post('/task/comment','TaskController@giveCommentsOnTasks')
    			->name('customer.task.comment');

    Route::get('/contacts','ContactController@contactListPage')
    			->name('customer.contact.show');

    Route::get('/contacts/add','ContactController@contactAddPage')
    			->name('customer.contact.add');
    
    Route::post('/contacts/save','ContactController@saveContact')
    			->name('customer.contact.save'); 

    Route::post('/contacts/update','ContactController@updateContact')
                ->name('customer.contact.update'); 

    Route::get('/weblogin','WebLoginsController@showWebLoginPage')
                ->name('customer.weblogin.show');

    Route::get('/weblogin/add','WebLoginsController@addWebLoginPage')
                ->name('customer.weblogin.add');

    Route::post('/weblogin/save','WebLoginsController@saveWebLogins')
                ->name('customer.weblogin.save');

    Route::post('/weblogin/update','WebLoginsController@updateWebLogins')
                ->name('customer.weblogin.update'); 

});


// });

//  Customer Routes End



