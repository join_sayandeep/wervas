<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TaskAssignProcess extends Mailable
{
    use Queueable, SerializesModels;
    public $assigned_associate, $task_no;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($assigned_associate,$task_no)
    {
        $this->assigned_associate = $assigned_associate;
        $this->task_no = $task_no;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.pages.mail.taskAssignProcess');
    }
}
