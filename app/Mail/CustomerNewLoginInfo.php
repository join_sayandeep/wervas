<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerNewLoginInfo extends Mailable
{
    use Queueable, SerializesModels;
    public $username,$temp_password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username,$temp_password)
    {
        $this->username = $username;
        $this->temp_password = $temp_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.pages.mail.customerNewLoginMail');
    }
}
