<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TaskCreationNotification extends Mailable
{
    use Queueable, SerializesModels;
    public $task_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($task_data)
    {
        $this->task_data = $task_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('common.mailers.taskCreationNotification');
    }
}
