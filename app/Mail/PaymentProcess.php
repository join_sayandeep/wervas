<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentProcess extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $token,$package_details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$token,$package_details)
    {
        $this->user = $user;
        $this->token = $token;
        $this->package_details = $package_details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('customer.pages.paymentProcessMail');
    }
}
