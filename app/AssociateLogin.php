<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssociateLogin extends Model
{
    protected $table = 'associate_login';
}
