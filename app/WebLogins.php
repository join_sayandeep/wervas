<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebLogins extends Model
{
    protected $table = "web_logins";
}
