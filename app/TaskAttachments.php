<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAttachments extends Model
{
    protected $table ='task_attachments';
}
