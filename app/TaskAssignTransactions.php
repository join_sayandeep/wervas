<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAssignTransactions extends Model
{
    protected $table = 'task_assign_transactions';
}
