<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packages;
use Illuminate\Support\Facades\Log;

class PackagesController extends Controller
{
    public function addPackagesView()
    {
       return view('admin.pages.packagesView');
    }

    public function savePackages(Request $request)
    {		
    	    $request->validate([
                'plan_name' => 'required',
                'validity_in_month' => 'numeric',
                'description' => 'max:300',
                'paypal_link' => 'url',
                'total_point_charged' => 'numeric',
            ], [
                'plan_name.required' => 'Plan name is required',
                'paypal_link.url' => 'Paypal link will be a url',
                'validity_in_month.numeric' => 'Numeric value is required',
                'description.max:300' => 'Description has max 300 characters',
                'total_point_charged.numeric' => 'Points should be in numeric format',

            ]);


    	    $plan_name = $request->input('plan_name');
			$validity_in_month = $request->input('validity_in_month');
			$description = $request->input('description');
			$price = $request->input('price');
			$paypal_link = $request->input('paypal_link');
			$status = $request->input('status');
            $total_point_charged = $request->input('total_point_charged');
			$flag = 0;
			if($status){

			   $flag = 1; 

			}else{

			   $flag = 0;
			}

			$savePackages = new Packages();

			$savePackages->plan_name = $plan_name;
			$savePackages->description = $description;
			$savePackages->validity_in_days = $validity_in_month;
			$savePackages->paypal_url = $paypal_link;
            $savePackages->total_point_charged = $total_point_charged;
			$savePackages->status = $flag;
			$savePackages->package_amount = $price;
			$savePackages->save();

			\Session::flash('message',"Package added successfully");
			\Session::flash('alert-class','alert-success');

            Log::notice("Package added by ". \Session::get('admin_uname'));

			return redirect()->back();

    }

    public function listOfPackages()
    {
    	 $package_data = Packages::orderBy('created_at', 'desc')
    	 						   ->paginate(10);

    	 return view('admin.pages.packagesList')
    	 			  ->with('package_data',$package_data);
    }

    public function changePackageStatus($status,$id)
    {
    	if($status == '0'){

    		$package_data = Packages::find($id);
    		$package_data->status = 0;
    		$package_data->save();

    	}else{
    		$package_data = Packages::find($id);
    		$package_data->status = 1;
    		$package_data->save();

    	}

    	return redirect()->back();
    }

    public function updatePackages(Request $request)
    {
    	$request->validate([
                'plan_name' => 'required',
                'validity_in_month' => 'numeric',
                'description' => 'max:300',
                'paypal_link' => 'url'
            ], [
                'plan_name.required' => 'Plan name is required',
                'paypal_link.url' => 'Paypal link will be a url',
                'validity_in_month.numeric' => 'Numeric value is required',
                'description.max:300' => 'Description has max 300 characters'
            ]);


    	    $plan_name = $request->input('plan_name');
			$validity_in_month = $request->input('validity_in_month');
			$description = $request->input('description');
			$price = $request->input('price');
			$paypal_link = $request->input('paypal_link');
			$status = $request->input('status');
			$id = $request->input('id');
			$flag = 0;


			if($status){

			   $flag = 1; 

			}else{

			   $flag = 0;
			}

		    $package_data = Packages::find($id);
            $package_data->plan_name = $plan_name;
            $package_data->description = $description;
            $package_data->validity_in_days = $validity_in_month;
            $package_data->paypal_url = $paypal_link;
            $package_data->package_amount = $price;
            $package_data->status = $flag;
            $package_data->save();


            \Session::flash('message',"Package updated successfully");
			\Session::flash('alert-class','alert-success');

			return redirect()->back();

    }
}
