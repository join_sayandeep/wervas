<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\CustomerAccount;
use App\Packages;
use App\Mail\PaymentProcess;
use App\CustomerLogin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CustomerAuthController extends Controller
{
    public function signUpView()
    {
    	return view('customer.pages.register');
    }

     public function signInView()
    {
    	return view('customer.pages.login');
    }
    

    public function signUpService(Request $request)
    {
    	$validator = Validator::make($request->all(),[
     			
     			'salutation' => 'required',
				'fname'    => 'required',
				'lname'    => 'required',
				'username' => 'required|alpha_num|min: 5|unique:customer_account',
				'contact_number' => 'required|digits:10|integer|unique:customer_account',
				'email'     => 'required|email|unique:customer_account',
				'shipping_address' => 'required',
				'plan_package'    => 'required|not_in:--- Select a plan ---'

    	]);

    	$salutation    = $request->input('salutation');
		$fname    = $request->input('fname');
		$lname    = $request->input('lname');
		$username    = $request->input('username');
		$contact_number    = $request->input('contact_number');
		$email    = $request->input('email');
		$shipping_address    = $request->input('shipping_address');
		$plan_package    = $request->input('plan_package');
 
		if($validator->fails()){
			  return redirect('/')
			  				->withInput()
			  				->withErrors($validator);
		}else{

			$customer_data = new CustomerAccount();
			$customer_data->username = $username;
			$customer_data->salutation = $salutation;
			$customer_data->fname = $fname;
			$customer_data->lname = $lname;
			$customer_data->contact_number = $contact_number;
			$customer_data->email = $email;
			$customer_data->shipping_address = $shipping_address;
			$customer_data->current_plan = $plan_package;
			$customer_data->account_status = '0';
			$customer_data->save();

			$package_details = Packages::where('id',$plan_package)->first();

			\Mail::to($email)->send(new PaymentProcess($username,$fname,$package_details));

			\Session::flash('message','Registration Successful. Please check your mailbox');
			\Session::flash('status','success');

			return redirect('/');
		}
    }

     public function signInService(Request $request)
    {
    	 $username = $request->input('username');
    	 $password = $request->input('password');

    	 if(empty($username) || empty($password)){

    	 	\Session::flash('message', "You have to fill the Username and Password");
    	 	\Session::flash('alert-class','alert-danger');

            Log::notice("Try to attempt to login without username and password");

    	 	return redirect()->back();

	        }else{

	    	 	   $check_username = CustomerLogin::where('username', $username)->first();
	    	 	   $customer_name = CustomerAccount::where('username', $username)->first();

	    	 	   if(!empty($check_username))
	    	 	   	{		
	    	 	   		    if($customer_name->account_status == '1'){


				    	 	   		if(Hash::check($password,$check_username['password'])){

				    	 	   			
				    	 	   			\Session::put('customer_uname',$username);

				                        Log::notice("Customer Login ".'- '. $customer_name['fname']." ".$customer_name['lname']);


				    	 	   					\Session::put('customer_name',$customer_name['fname']." ".$customer_name['lname']);
				                         	    return redirect(route('customer.dashboard'));
				                 
				    	 	   		}else {

				    	 	   			\Session::flash('message', "Wrong Username and Password");
							    	 	\Session::flash('alert-class','alert-danger');

				                         Log::warning("Not Valid Info");

							    	 	return redirect()->back();

				    	 	   		}


				    	 	}else{


				    	 	   			\Session::flash('message', "Account Blocked. Contact with Administrator");
							    	 	\Session::flash('alert-class','alert-danger');

				                         Log::warning("Account Blocked");

							    	 	return redirect()->back();

				    	 	}

	    	 	   	}else{

	    	 	   		\Session::flash('message', "Credentials are not valid");
			    	 	\Session::flash('alert-class','alert-danger');

	                     Log::warning("Not Valid Info");

			    	 	return redirect()->back();
	    	 	   	}

	    	 }
    }


    // public function resetFirstTimeLoginPassword(Request $request)
    // {

    // 	$validator = Validator::make($request->all(),[
     			
    //  			'customer_uname' => 'required|exists:customer_account,username',
				// 'old_password'    => 'required',
				// 'new_password'    => 'required',
				// 're_password' => 'required',

    // 	]);


    // 	$customer_uname = $request->input('customer_uname');
    // 	$old_password = $request->input('old_password');
    // 	$new_password = $request->input('new_password');
    // 	$re_password = $request->input('re_password');

    // 	return view('admin.pages.password_reset_first_time');
    // }

    

}
