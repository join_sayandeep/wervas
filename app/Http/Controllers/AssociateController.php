<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Associates;
use App\AssociateLogin;
use App\Mail\CreateAssociatesNewAccount;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\TaskAssignTransactions;
use App\Tasks;
use App\Mail\TaskAssignProcess;


class AssociateController extends Controller
{
    public function addAssociateView()
    {
    	return view('admin.pages.addAssociate');
    }

    public function saveAssociate(Request $request)
    {
    	 $validator = Validator::make($request->all(),[
     			
				'fname'    => 'required',
				'lname'    => 'required',
				'username' => 'required|alpha_num|min: 5|unique:associates,username',
				'contact_number' => 'required|digits:10|integer|unique:associates,contact_number',
				'email'     => 'required|email|unique:associates,email',
				'address' => 'required',
				'doj'    => 'required'

	    	]);

			$fname    = $request->input('fname');
			$lname    = $request->input('lname');
			$username    = $request->input('username');
			$contact_number    = $request->input('contact_number');
			$email    = $request->input('email');
			$address    = $request->input('address');
			$doj    = $request->input('doj');
	 
			if($validator->fails()){
				  return redirect(route('admin.view.associate'))
				  				->withInput()
				  				->withErrors($validator);
			}else{

				// Password generating for associates

	    		$random = Str::random(10);
				$pass=rand(1,999).$random.rand(1,999);
			    $temp_password=str_shuffle($pass);

			    $hashed_temp_password = Hash::make($temp_password);

				$associate_data = new Associates();
				$associate_data->username = $username;
				$associate_data->fname = $fname;
				$associate_data->lname = $lname;
				$associate_data->contact_number = $contact_number;
				$associate_data->email = $email;
				$associate_data->address = $address;
				$associate_data->doj = $doj;
				$associate_data->account_status = '1';
				$associate_data->save();


				$associate_logindata = new AssociateLogin();
				$associate_logindata->username = $username;
				$associate_logindata->password = $hashed_temp_password;
				$associate_logindata->save();


				\Mail::to($email)->send(new CreateAssociatesNewAccount($username,$temp_password));

				\Session::flash('message','Registration Successful. Please check associate mailbox');
				\Session::flash('alert-class','alert-success');

				return redirect()->back();
    		}
    }

    public function listOfAssociates()
    {
    	$associate_data = Associates::orderBy('created_at', 'desc')
    								->paginate(10);

    	return view('admin.pages.associateList')
    				->with('associate_data', $associate_data);
    }

    public static function Associates()
    {
    	$associate_data = Associates::all();

    	return $associate_data;
    }

    public function updateAssociateAccountStatus(Request $request)
    {
    	 $id = $request->input('id');
    	 $status = $request->input('status');

    	 $associate_account_status = Associates::find($id);
    	 $associate_account_status->account_status = $status;
    	 $associate_account_status->save();

    	 \Session::flash('message','Account Status Updated Successfully');
		 \Session::flash('alert-class','success');

		  return redirect()->back();
    }


    public function assignTaskToAssociate(Request $request)
    {
    	$assigned_associate = $request->input('assigned_associate');
        $last_modified    =    Carbon::now()->toDateTimeString();
        $id = $request->input('id');
        $task_no = $request->input('task_no');

         $validator = Validator::make($request->all(),[
					        			'assigned_associate' => 'required|exists:associates,username',
					        			'id' => 'required',
					        			'task_no' => 'required'

					        ]);

         	if($validator->fails()){
				  		return redirect(route('customer.task.view'))
					  				->withInput()
					  				->withErrors($validator);
				  	}else{


				  		$task_data = Tasks::where('task_no',$task_no)
				  							->where('id',$id)
				  							->update(['assigned_associate'=>$assigned_associate,
				  									  'last_modified'=>$last_modified,
				  									  'task_status'=>999]);

				  	    $task_entry_transaction = new TaskAssignTransactions();
				  	    $task_entry_transaction->task_no = $task_no;
				  	    $task_entry_transaction->associate_uname = $assigned_associate;
				  	    $task_entry_transaction->save();

				  	    $associate_data = Associates::where('username',$assigned_associate)->first();


				  	    \Mail::to($associate_data->email)->send(new TaskAssignProcess($assigned_associate,$task_no));

				  	    \Session::flash('message','Assigned task');
				  	    \Session::flash('alert-class','alert-success');

				  	    return redirect()->back();

				  	}
    }
}
