<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Associates;
use App\AssociateLogin;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AssociateAuthController extends Controller
{
    public function signInView()
    {
    	return view('associate.pages.login');
    }

    public function dashboardView(){
    	return view('associate.pages.home');
    }

    public function signInService(Request $request){

    	 $validator = Validator::make($request->all(),[
     			
				'username'    => 'required|exists:associate_login,username',
				'password'    => 'required'

	    	]);

			$username    = $request->input('username');
			$password    = $request->input('password');

			if($validator->fails()){
				return redirect(route('assoc.login.view'))
				  				->withInput()
				  				->withErrors($validator);
			}else{

					$assoc_data_verify = Associates::where('username', $username)
													 ->first();

					if(empty($assoc_data_verify)){

						 \Session::flash('message',"Wrong username or password");
						 \Session::flash('alert-class','alert-danger');

						 return redirect(route('assoc.login.view'));
					}else{

						 if($assoc_data_verify->account_status == 1){

						 		 $verify_pass = AssociateLogin::where('username', $username)->first();

						 		  if(Hash::check($password,$verify_pass->password)){

						 		  	   		\Session::put('assoc_uname',$assoc_data_verify->username);
    	 	   								\Session::put('assoc_name',$assoc_data_verify->fname.' '.$assoc_data_verify->lname);
						 		  
    	 	   								return redirect(route('assoc.dashboard'));
						 		  }else{
						 		  		\Session::flash('message',"Wrong username or password");
										 \Session::flash('alert-class','alert-danger');

										 return redirect(route('assoc.login.view'));
						 		  }


						 }else{

						 	\Session::flash('message',"Account is suspended. Contact your Administrator!");
							 \Session::flash('alert-class','alert-danger');

							 return redirect(route('assoc.login.view'));
						 }
					}
			}

    }
}
