<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacts;
use Validator;

class ContactController extends Controller
{
    	public function contactListPage()
    	{
    		$contact_data = Contacts::orderBy('created_at', 'desc')
    									->paginate(10);

    		return view('customer.pages.contactsList')
    					->with('contacts',$contact_data);
    	}

    	public function contactAddPage()
    	{
    		return view('customer.pages.createContactView');
    	}


    	public function saveContact(Request $request)
    	{
    		 $validator = Validator::make($request->all(),[
     			
									'name'    => 'required',
									'phone'    => 'required|digits:10',
									'email' => 'required|email',
									'relation' => 'required',
									'address'     => 'required'

						    	]);

					$name    = $request->input('name');
					$phone    = $request->input('phone');
					$email    = $request->input('email');
					$relation    = $request->input('relation');
					$address    = $request->input('address');
			 
					if($validator->fails()){
						  return redirect(route('customer.contact.add'))
						  				->withInput()
						  				->withErrors($validator);
					}else{

							try{
									$save_contact_data = new Contacts();
									$save_contact_data->name = $name;
									$save_contact_data->phone = $phone;
									$save_contact_data->relation =  $relation;
									$save_contact_data->email = $email;
									$save_contact_data->address = $address;
									$save_contact_data->save();

									\Session::flash('message','Contact created successfully');
								    \Session::flash('alert-class','alert-success');

								return redirect()->back();

							}catch(\Exception $e){

								\Session::flash('message','Exception occurred');
								\Session::flash('alert-class','alert-danger');

								return redirect()->back();
							}
					}
    	}

    	public function updateContact(Request $request)
    	{
    			 $validator = Validator::make($request->all(),[
     			
									'name'    => 'required',
									'phone'    => 'required|digits:10',
									'email' => 'required|email',
									'relation' => 'required',
									'address'     => 'required'

						    	]);

					$name    = $request->input('name');
					$phone    = $request->input('phone');
					$email    = $request->input('email');
					$relation    = $request->input('relation');
					$address    = $request->input('address');
					$id    = $request->input('conid');
			 
					if($validator->fails()){
						  return redirect(route('customer.contact.show'))
						  				->withInput()
						  				->withErrors($validator);
					}else{

							try{
								 $contact_update_data = Contacts::find($id);
								 $contact_update_data->name = $name;
								 $contact_update_data->phone = $phone;
								 $contact_update_data->email = $email;
								 $contact_update_data->relation = $relation;
								 $contact_update_data->address = $address;
								 $contact_update_data->save();


								 \Session::flash('message','Contact updated successfully');
								  \Session::flash('alert-class','alert-success');

								return redirect()->back();

							}catch(\Exception $e){

								\Session::flash('message','Exception occurred');
								\Session::flash('alert-class','alert-danger');

								return redirect()->back();
							}
					}
    	}

    	
}
