<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Tasks;
use App\CustomerAccount;
use App\Mail\TaskCreationNotification;
use Carbon\Carbon;
use App\TaskAttachments;
use Illuminate\Support\Facades\DB;
use App\TaskComments;

class TaskController extends Controller
{
	

    public function taskViewPage()
    {
    	return view('customer.pages.tasksEntry');
    }

    public function taskCreate(Request $request)
    {
		$task_priority = $request->input('task_priority');
		$customer_email = $request->input('customer_email');
		$task_type    = $request->input('task_type');
		$subject = $request->input('subject');
		$resolutions	    = $request->input('resolutions');
		$comments    = $request->input('comments');
		$description    = $request->input('description');
	
		$task_no = Carbon::now()->format('Y').''.Carbon::now()->format('M').''.mt_rand(1000,9999);


        $validator = Validator::make($request->all(),[
        			'task_priority' => 'required',
        			'customer_email' => 'required|email',
        			'subject' => 'required',
        			'task_type' => 'nullable',
        			'comments' => 'nullable',
        			'description' => 'required',
        			'task_attachment.*' => 'nullable|max:3000|mimes:jpg,jpeg,png,pdf'
        ]);

       			 if($validator->fails()){
				  		return redirect(route('customer.task.view'))
					  				->withInput()
					  				->withErrors($validator);
				  	}else{
				  			$attachment_name=array();
				  			$task_attachment=array();
				  			if($request->hasFile('task_attachment')){
				  				$task_attachment    = $request->file('task_attachment');
				  			 foreach($task_attachment as $attach) {
				  				    $filename = $attach->getClientOriginalName();
						            $attachment_name[] = $filename;
						            $path = '/taskAttachments/';
						            $attach->storeAs($path,$filename, 'local'); // saving the file in temp storage
						            
						            #unlink(storage_path('app/taskAttachments/'.$attachment_name)); die; // deleting the file
						        }

									foreach($attachment_name as $attach) {
											$save_attachments = new TaskAttachments();
											$save_attachments->task_no = $task_no;
											$save_attachments->customer_email = $customer_email;
											$save_attachments->task_attachments = $attach;
											$save_attachments->save();
									 }
							}else{

								$attachment_name  = "";
							}

							$task_data = new Tasks();
							$task_data->task_no = $task_no;
							$task_data->task_priority = $task_priority;
							$task_data->customer_email = $customer_email;
							$task_data->task_type = $task_type;
							$task_data->subject = $subject;
							$task_data->comments = $comments;
							$task_data->description = $description;
							$task_data->created_by = \Session::get('customer_uname');
							$task_data->task_status = 1; #Open Task
							$task_data->save();

							//sending mail notification to customer and admin
							\Mail::to($customer_email)->send(new TaskCreationNotification($task_data));

							$admin_email ='smajumdar1993@gmail.com';
							\Mail::to($admin_email)->send(new TaskCreationNotification($task_data));


							\Session::flash('message','Task has been created. See in task table');
							\Session::flash('alert-class','alert-success');


							return redirect()->back();


				  	}
		
    }


    public function tasksList()
    {
    	$customer_uname = \Session::get('customer_uname');

    	$tasks_list_data =  DB::table('tasks')
					            ->orderBy('created_at', 'desc')
					    		->paginate(10);


    	return view('customer.pages.tasksList')->with('tasks_data', $tasks_list_data);
    }


    public function displayAttachment($filename)
    {
    	// $path = storage_public('taskAttachments/' . $filename);
    	 $path = storage_path('app/taskAttachments/'.$filename);

        if (!\File::exists($path)) {
		        abort(404);
		    }
			
			$file = \File::get($path);

		    $type = \File::mimeType($path);

		    $response = \Response::make($file, 200);

		    $response->header("Content-Type", $type);

		    return $response;
    }

    public function updateTask(Request $request)
    {
    	// $task_no = $request->input('task_no');
		$task_priority = $request->input('task_priority');
		$customer_email = $request->input('customer_email');
		$task_type    = $request->input('task_type');
		$subject = $request->input('subject');
		$resolutions	    = $request->input('resolutions');
		$comments    = $request->input('comments');
		$description    = $request->input('description');

		$tid = $request->input('tid');

        $validator = Validator::make($request->all(),[
        			'task_priority' => 'required',
        			'customer_email' => 'required|email',
        			'subject' => 'required',
        			'task_type' => 'required',
        			'resolutions' => 'required',
        			'comments' => 'required',
        			'description' => 'required',
        			'tid' => 'required',
        			'task_attachment' => 'max:30000|mimes:jpg,jpeg,png,pdf'

        ]);

       			 if($validator->fails()){
				  		return redirect(route('customer.task.list'))
					  				->withInput()
					  				->withErrors($validator);
				  	}else{

				  			#get the old attachment file name
				  			$old_attachment_name = Tasks::where('id', $tid)->first()->attachment;

				  			if($request->hasFile('task_attachment')){
				  				
				  				// deleting the old file
					            unlink(storage_path('app/taskAttachments/'.$old_attachment_name)); 


				  				$task_attachment    = $request->file('task_attachment');
					            $attachment_name = $task_attachment->getClientOriginalName();
					            $path = '/taskAttachments/';
					            $task_attachment->storeAs($path,$attachment_name, 'local'); // saving the file in temp storage


							}else{

								$attachment_name  = $old_attachment_name;
							}


							$task_data = Tasks::find($tid);
							$task_data->task_priority = $task_priority;
							$task_data->customer_email = $customer_email;
							$task_data->task_type = $task_type;
							$task_data->subject = $subject;
							$task_data->resolutions = $resolutions;
							$task_data->comments = $comments;
							$task_data->description = $description;
							$task_data->attachment = $attachment_name;
							$task_data->created_by = \Session::get('customer_uname');
							$task_data->task_status = 1; #Open Task
							$task_data->save();

							\Session::flash('message','Task has been updated');
							\Session::flash('alert-class','alert-success');


							return redirect()->back();


				  	}
    }


    public function deleteTask(Request $request)
    {
    	$tid = $request->input('tid');

    	$delete_task_data = Tasks::find($tid);
    	$delete_task_data->delete();

    	\Session::flash('message','Task has been deleted');
		\Session::flash('alert-class','alert-success');

        return redirect()->back();
    }



    public function listOfTasksForAdmin()
    {	
    	$task_data = Tasks::orderBy('created_at', 'desc')->paginate(10);

    	return view('admin.pages.taskList')->with('task_data', $task_data);
    }

    public function taskInfoForAdmin($id)
    {
    	$task_data = DB::table('tasks')
					      ->first();

	    $task_id_data = DB::table('tasks')
					      ->where('id', $id)
					      ->first();

		$task_attachment_data = DB::table('task_attachments')
					      ->where('task_no',$task_id_data->task_no)
					      ->get();


    	return view('admin.pages.taskDetails')
    				->with('task_data', $task_data)
    				->with('task_attachment_data', $task_attachment_data);
    }

    public function taskInfoForAssociates($id)
    {
    	$task_data = DB::table('tasks')
					      ->first();

	    $task_id_data = DB::table('tasks')
					      ->where('id', $id)
					      ->first();

		$task_attachment_data = DB::table('task_attachments')
					      ->where('task_no',$task_id_data->task_no)
					      ->get();


    	return view('associate.pages.taskDetails')
    				->with('task_data', $task_data)
    				->with('task_attachment_data', $task_attachment_data);
    }

    public function taskInfoForCustomer($id)
    {
    	$task_data = DB::table('tasks')
					      ->first();

	    $task_id_data = DB::table('tasks')
					      ->where('id', $id)
					      ->first();

		$task_attachment_data = DB::table('task_attachments')
					      ->where('task_no',$task_id_data->task_no)
					      ->get();

    	return view('customer.pages.taskDetails')
    				->with('task_data', $task_data)
    				->with('task_attachment_data', $task_attachment_data);
    }

    public function getListOfTasksForAssoc()
    {	
    	$task_data = Tasks::orderBy('created_at', 'desc')->paginate(10);

    	return view('associate.pages.tasksList')->with('task_data', $task_data);
    }


	public function giveCommentsOnTasks(Request $request){
			
				$timezone = \Config::get('app.timezone');
				$timestamp = Carbon::now()->timezone($timezone)->toDateTimeString();
				$uuid = \Uuid::generate();
				$comments = $request->input('comments');
				$task_no    = $request->input('task_no');
				$reply_from    = $request->input('reply_from');

				$validator = Validator::make($request->all(),[
     			
										'task_no' => 'required',
										'reply_from'    => 'required',
										'comments'    => 'required'
						
									]);

			   if($validator->fails()){
				return redirect(route('customer.task.info',$task_no))
								->withInput()
								->withErrors($validator);
		  		}else{
					try{
						$comments_save = new TaskComments();
						$comments_save->uuid = $uuid;
						$comments_save->task_no = $task_no;
						$comments_save->comments = $comments;
						$comments_save->reply_from = $reply_from;
						$comments_save->timestamp = $timestamp;
						$comments_save->save();

						\Session::flash('message', "Comment done successfully");
						\Session::flash('alert-class','alert-success');
						return redirect()->back();
					}catch(\Exception $e){
							\Session::flash('message',"Exception Occured");
							\Session::flash('alert-class','alert-danger');
						    return redirect()->back();
					}

				  }
	}
}
