<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerAccount;
use App\Packages;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\CustomerLogin;
use App\Mail\CustomerNewLoginInfo;
use Validator;
use Illuminate\Support\Str;
use App\Mail\PaymentProcess;

class CustomerController extends Controller
{
    public function customerListInAdmin()
    {
    	$list_of_customers = CustomerAccount::orderBy("created_at","desc")
    										  ->paginate(10);
        $packages_list = Packages::orderBy("created_at","desc")->get();

    	return view('admin.pages.customerList')
    				->with("customer_data", $list_of_customers)
    				->with("packages_list", $packages_list);
    }

    public function customerAcceptance(Request $request)
    {
    		$request->validate([
                'next_payment_date' => 'required',
                'id' => 'required',
                'uname' => 'required',
                'email' => 'required'
            ], [
                'next_payment_date.required' => 'Date is required',
                'id.required' => 'Something Went Wrong'
            ]);

    		$id = $request->input('id');
    		$next_payment_date = $request->input('next_payment_date');
    		$username = $request->input('uname');
    		$email = $request->input('email');

    		// update next date payment and account status
    		$update_customer_account = CustomerAccount::find($id);
    		$update_customer_account->next_payment_date = $next_payment_date;
    		$update_customer_account->account_status = 1;
    		$update_customer_account->save();

    		// Password generating for client/customer

    		$random = Str::random(10);
			$pass=rand(1,999).$random.rand(1,999);
		    $temp_password=str_shuffle($pass);

		    $hashed_temp_password = Hash::make($temp_password);

		    $save_new_customer_login_info = new CustomerLogin();
		    $save_new_customer_login_info->username = $username;
		    $save_new_customer_login_info->password = $hashed_temp_password;
		    $save_new_customer_login_info->first_time_login = 1;
		    $save_new_customer_login_info->save();

		    // Now mail the new password to the customer

		    \Mail::to($email)->send(new CustomerNewLoginInfo($username,$temp_password));


            \Session::flash('message',"Customer Accepted");
			\Session::flash('alert-class','alert-success');

			return redirect()->back();

    }

    public function singleCustomerProfile($username)
    {	
    	$single_customer_data = CustomerAccount::where('username',$username)
    											->first();

        $packages_list = Packages::orderBy("created_at","desc")->get();

    	return view('admin.pages.singleCustomerProfileDetails')
    				->with('single_customer',$single_customer_data)
    				->with('packages_list',$packages_list);
    }

    public function singleCustomerProfileStatusUpdate($username,$status)
    {
    	if($status == 2){
    			$update_single_customer_account_status = CustomerAccount::where('username',$username)
    													->update(['account_status'=>2]);
    	}else if($status == 1){
    			$update_single_customer_account_status = CustomerAccount::where('username',$username)
    													->update(['account_status'=>1]);
    	}else{
    			$update_single_customer_account_status = CustomerAccount::where('username',$username)
    													->update(['account_status'=>1]);

    	}
    	
    	return redirect()->back();
    }

    public function createCustomerAccount()
    {
    	return view('admin.pages.customerCreate');
    }

    public function saveCustomerAccountInfo(Request $request)
    {
    	$validator = Validator::make($request->all(),[
     			
     			'salutation' => 'required',
				'fname'    => 'required',
				'lname'    => 'required',
				'username' => 'required|alpha_num|min: 5|unique:customer_account,username',
				'contact_number' => 'required|digits:10|integer|unique:customer_account,contact_number',
				'email'     => 'required|email|unique:customer_account,email',
				'shipping_address' => 'required',
				'plan_package'    => 'required|not_in:--- Select a plan ---'

	    	]);

	    	$salutation    = $request->input('salutation');
			$fname    = $request->input('fname');
			$lname    = $request->input('lname');
			$username    = $request->input('username');
			$contact_number    = $request->input('contact_number');
			$email    = $request->input('email');
			$shipping_address    = $request->input('shipping_address');
			$plan_package    = $request->input('plan_package');
	 
			if($validator->fails()){
				  return redirect(route('admin.customer.create'))
				  				->withInput()
				  				->withErrors($validator);
			}else{

				$customer_data = new CustomerAccount();
				$customer_data->username = $username;
				$customer_data->salutation = $salutation;
				$customer_data->fname = $fname;
				$customer_data->lname = $lname;
				$customer_data->contact_number = $contact_number;
				$customer_data->email = $email;
				$customer_data->shipping_address = $shipping_address;
				$customer_data->current_plan = $plan_package;
				$customer_data->account_status = '0';
				$customer_data->save();

				$package_details = Packages::where('id',$plan_package)->first();

				\Mail::to($email)->send(new PaymentProcess($username,$fname,$package_details));

				\Session::flash('message','Registration Successful. Please check client mailbox for Payment Process');
				\Session::flash('alert-class','success');

				return redirect()->back();
    		}
	}

    public function dashboardView()
    {
        return view('customer.pages.home');
    }
}
