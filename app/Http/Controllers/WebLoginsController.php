<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebLogins;
use Validator;

class WebLoginsController extends Controller
{
    public function showWebLoginPage()
    {	
    	$added_by = \Session::get('customer_uname'); 

    	$weblogins_list = WebLogins::where('added_by', $added_by)
    								 ->orderBy('created_at', 'desc')
    								 ->paginate(10);

  		return view('customer.pages.webLogins')
  					->with('webLogData',$weblogins_list);
    } 


    public function	addWebLoginPage()
    {
    	return view('customer.pages.addWebLoginPage');
    }

    public function	saveWebLogins(Request $request)
    {
    	$added_by = \Session::get('customer_uname');

    	$validator = Validator::make($request->all(),[

									'username' => 'required',
									'password' => 'required',
									'web_url'     => 'required|url'
						    	]);

    	   $username = $request->input('username');		
		   $password = $request->input('password');	
	       $web_url  = $request->input('web_url');

	    if($validator->fails()){
				  return redirect(route('customer.weblogin.add'))
				  				->withInput()
				  				->withErrors($validator);
			}else{

					try{
							$weblogin_data = new WebLogins();
							$weblogin_data->username = $username;
							$weblogin_data->password = $password;
							$weblogin_data->web_url = $web_url;
							$weblogin_data->added_by = $added_by;
							$weblogin_data->save();

							\Session::flash('message','Record Successfully added');
							\Session::flash('alert-class','alert-success');
						    return redirect(route('customer.weblogin.add'));

					}catch(\Exception $e){
							
							\Session::flash('message',$e->getMessage());
							\Session::flash('alert-class','alert-danger');
						    return redirect(route('customer.weblogin.add'));
					}

			}
    }

    public function updateWebLogins(Request $request)
    {

    	$validator = Validator::make($request->all(),[

									'username' => 'required',
									'password' => 'required',
									'web_url'     => 'required|url',
									'id'     => 'required',
						    	]);

    	   $username = $request->input('username');		
		   $password = $request->input('password');	
	       $web_url  = $request->input('web_url');
	       $id  = $request->input('id');

	    if($validator->fails()){
				  return redirect(route('customer.weblogin.show'))
				  				->withInput()
				  				->withErrors($validator);
			}else{

					try{
							$weblogin_data = WebLogins::find($id);
							$weblogin_data->username = $username;
							$weblogin_data->password = $password;
							$weblogin_data->web_url = $web_url;
							$weblogin_data->save();

							\Session::flash('message','Record Successfully added');
							\Session::flash('alert-class','alert-success');
						    return redirect(route('customer.weblogin.show'));

					}catch(\Exception $e){
							
							\Session::flash('message',$e->getMessage());
							\Session::flash('alert-class','alert-danger');
						    return redirect(route('customer.weblogin.show'));
					}

			}
    }
}
