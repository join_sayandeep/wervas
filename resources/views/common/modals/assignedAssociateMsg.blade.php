<div class="modal fade" id="assignedAssociates{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Assign to a Associate [Task# {{$dt->task_no}}]</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
             
               <div class="row">
                  <div class="col-md-10">
                    <div class="form-group" >
                 <h3> This Task has been assigned. Wait for confirmation.</h3>
                    </div>
                  </div>
                 </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->