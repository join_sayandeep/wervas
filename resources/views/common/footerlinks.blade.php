<!-- jQuery -->
<script src="/wervas/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/wervas/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/wervas/dist/js/adminlte.min.js"></script>


<!-- REQUIRED SCRIPTS -->
<!-- overlayScrollbars -->
<script src="/wervas/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="/wervas/dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="/wervas/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="/wervas/plugins/raphael/raphael.min.js"></script>
<script src="/wervas/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="/wervas/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="/wervas/plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="/wervas/dist/js/pages/dashboard2.js"></script>

<!-- bs-custom-file-input -->
<script src="/wervas/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="/wervas/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/wervas/dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

<!-- Select2 -->
<script src="/wervas/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="/wervas/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="/wervas/plugins/moment/moment.min.js"></script>
<script src="/wervas/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="/wervas/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="/wervas/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/wervas/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="/wervas/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
