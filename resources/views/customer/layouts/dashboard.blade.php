<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>WERVAS | Customer</title>

   @include('common.headerlinks')

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">

     @include('customer.include.navbar')

     @include('customer.include.sidebar')

       @yield('content')

     @include('common.footer')
</div>
<!-- ./wrapper -->
     @include('common.footerlinks')
</body>
</html>