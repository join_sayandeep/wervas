<div class="modal fade" id="contactEdit{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Update Contact </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <!-- form start -->
              <form role="form" action="{{ route('customer.contact.update') }}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="conid" value="{{$dt->id}}">
                <div class="card-body">
                 <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Name</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{$dt->name}}" required>
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Phone#</label>
                    <input type="text" class="form-control" placeholder="Phone Number" name="phone" value="{{$dt->phone}}" required>

                     @if ($errors->has('phone'))
                          <span class="text-danger">{{ $errors->first('phone') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Relation</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="text" class="form-control" placeholder="Relation" name="relation" value="{{$dt->relation}}" required>

                         @if ($errors->has('relation'))
                          <span class="text-danger">{{ $errors->first('relation') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Email</label>
                             <input type="text" class="form-control" placeholder="Email Address" name="email" value="{{$dt->email}}" required>

                              @if ($errors->has('email'))
                          <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                        </div>
                  </div>
                

                 </div>

                 <div class="row">
           

                  <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Address</label>
                             <input type="text" class="form-control" placeholder="Address" name="address" value="{{$dt->address}}" required>

                              @if ($errors->has('address'))
                          <span class="text-danger">{{ $errors->first('address') }}</span>
                      @endif

                        </div>
                  </div>
                 </div>


            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Update Contact</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->