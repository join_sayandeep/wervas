<div class="modal fade" id="webLoginEdit{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Update Web Login </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <!-- form start -->
              <form role="form" action="{{ route('customer.weblogin.update') }}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$dt->id}}">
                <div class="card-body">
                 <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Username</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="text" class="form-control" placeholder="Username" name="username" value="{{$dt->username}}" required>
                        @if ($errors->has('username'))
                          <span class="text-danger">{{ $errors->first('username') }}</span>
                      @endif

                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="text" class="form-control" placeholder="Password" name="password" value="{{$dt->password}}" required>

                     @if ($errors->has('password'))
                          <span class="text-danger">{{ $errors->first('password') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                    <label for="exampleInputFile">Web URL</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="text" class="form-control" placeholder="Web URL" name="web_url" value="{{$dt->web_url}}" required>

                         @if ($errors->has('web_url'))
                          <span class="text-danger">{{ $errors->first('web_url') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  </div>

                 </div>

             </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Update</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->