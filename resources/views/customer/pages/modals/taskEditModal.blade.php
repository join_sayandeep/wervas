<div class="modal fade" id="taskEdit{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Update Task# {{$dt->task_no}} </h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <!-- form start -->
              <form role="form" action="{{ route('customer.task.update') }}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="tid" value="{{$dt->id}}">
                <div class="card-body">
                 <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Task #</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="text" class="form-control" placeholder="Task#" name="task_no" value="{{$dt->task_no}}" disabled>
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Task Priority</label>
                    <input type="text" class="form-control" placeholder="Priority of this task" name="task_priority" value="{{$dt->task_priority}}" required>

                     @if ($errors->has('task_priority'))
                          <span class="text-danger">{{ $errors->first('task_priority') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Customer Email (Primary)</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="email" class="form-control" placeholder="Your email address" name="customer_email" value="{{$dt->customer_email}}" required>

                         @if ($errors->has('customer_email'))
                          <span class="text-danger">{{ $errors->first('customer_email') }}</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Task Type</label>
                             <input type="text" class="form-control" placeholder="Type of this task" name="task_type" value="{{$dt->task_type}}" required>

                              @if ($errors->has('task_type'))
                          <span class="text-danger">{{ $errors->first('task_type') }}</span>
                      @endif
                        </div>
                  </div>
                

                 </div>

                 <div class="row">
           

                  <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Subject</label>
                             <input type="text" class="form-control" placeholder="Subject" name="subject" value="{{$dt->subject}}" required>

                              @if ($errors->has('subject'))
                          <span class="text-danger">{{ $errors->first('subject') }}</span>
                      @endif

                        </div>
                  </div>
                 </div>

                 <div class="row">

                    <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Attachment</label>
                             <input type="file" class="form-control" name="task_attachment" accept=".jpg,.png,.pdf,.jpeg" >
                              @if ($errors->has('task_attachment'))
                          <span class="text-danger">{{ $errors->first('task_attachment') }}</span>
                      @endif
                        </div>

                      <p><a href="{{ route('attachment.display',$dt->attachment) }}" target="_blank">Attachment Link</a></p>
                  </div>


                 </div>

                 <div class="row">
            

                   <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Resolutions</label>
                             <textarea class="form-control" rows="3" maxlength="300" placeholder="Write your resolutions here...." name="resolutions" required>{{$dt->resolutions}}</textarea>
                              @if ($errors->has('resolutions'))
                          <span class="text-danger">{{ $errors->first('resolutions') }}</span>
                      @endif
                        </div>
                  </div>

                 </div>

                 <div class="row">
                     <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Comments</label>
                              <textarea class="form-control" rows="3" maxlength="400" placeholder="Write your comments here...." name="comments" required>{{$dt->comments}}</textarea>

                              @if ($errors->has('comments'))
                          <span class="text-danger">{{ $errors->first('comments') }}</span>
                      @endif

                        </div>
                  </div>
                 </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Description</label>
                             <textarea class="form-control" rows="3" placeholder="Write description here...." name="description" required>{{$dt->description}}</textarea>
                              @if ($errors->has('description'))
                          <span class="text-danger">{{ $errors->first('description') }}</span>
                      @endif
                        </div>
                  </div>

                 </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Update Task</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->