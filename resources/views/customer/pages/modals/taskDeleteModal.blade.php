<div class="modal fade" id="taskDelete{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Delete Task#  {{$dt->task_no}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{ route('customer.task.delete') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="tid" value="{{$dt->id}}">
               <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputEmail1 text-center">Are you sure?</label>
                    
                  </div>
                  </div>
                 </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <button type="submit" class="btn btn-secondary">Yes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->