<div class="modal fade" id="commentsontask{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Reply to [Task# {{$dt->task_no}}]</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <form role="form" action="{{ route('customer.task.comment') }}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="task_no" value="{{$dt->task_no}}">
                <input type="hidden" name="reply_from" value="{{Session::get('customer_uname')}}">
                <div class="modal-body">
             
               <div class="row">
                  <div class="col-md-10">
                    <div class="form-group" >
                    
                        <div class="input-group">
                        <label>Your Message</label> &nbsp;
                    
                            <textarea class="form-control" placeholder="Write Your Comment Here ...(Within 500 Characters)" name="comments" rows="3" maxlength="500" required></textarea>
                       
                        </div>
                    </div>
                  </div>
                 </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="submit" class="btn btn-primary">Add Comment</button>
            </div>
          
          </div>
          </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->