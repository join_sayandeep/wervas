@extends('customer.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Web Login Data</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('customer.weblogin.add')}}" type="button" class="btn btn-warning btn-sm" >Add Web Login</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
            
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Contacts</h3>
              </div>
              <!-- style="@media only screen and (max-width: 1900px) { displays: inline-table;}}" -->
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-responsive" style="display: inline-table;">
                  <thead>                  
                    <tr>
                      <th>Username/Email</th>
                      <th>Web URL</th>
                      <th>Password</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if(empty($webLogData))

                          <tr>
                          <td></td>
                           <td>No Data</td>
                           <td></td>
                            <td></td>
                          </tr>
                
                    @else

                             @foreach($webLogData as $dt)
                          <tr>
                            <td>{{ $dt->username }}</td>
                            <td>{{ $dt->web_url }}</td>
                            <td>{{ $dt->password }}</td>
                            <td>
                               <a href="#"type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#webLoginEdit{{$dt->id}}" >Edit</a>
                                @include('customer.pages.modals.webLoginEditModal',['dt'=>$dt])
                            </td>
                       
                          </tr>
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $webLogData->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
      
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





  </div>
  <!-- /.content-wrapper -->




@endsection