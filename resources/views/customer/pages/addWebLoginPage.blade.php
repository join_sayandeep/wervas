@extends('customer.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Web Login</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{route('customer.weblogin.show')}}"type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">New WebLogin</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('customer.weblogin.save') }}" method="POST">
              	{{csrf_field()}}
                <div class="card-body">
                 <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Username/Email</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="text" class="form-control" placeholder="Username/Email Address" name="username" value="{{old('username')}}" required>
                         @if ($errors->has('username'))
                          <span class="text-danger">{{ $errors->first('username') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="text" class="form-control" placeholder="Password" name="password" value="{{old('password')}}" required>

                     @if ($errors->has('password'))
                          <span class="text-danger">{{ $errors->first('password') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Web URL</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="text" class="form-control" placeholder="URL" name="web_url" value="{{old('web_url')}}" required>

                         @if ($errors->has('web_url'))
                          <span class="text-danger">{{ $errors->first('web_url') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                 </div>

                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection