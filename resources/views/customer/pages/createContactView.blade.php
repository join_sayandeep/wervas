@extends('customer.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Save Contact</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{route('customer.contact.show')}}"type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">New Contact</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('customer.contact.save') }}" method="POST">
              	{{csrf_field()}}
                <div class="card-body">
                 <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Name</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{old('name')}}" required>
                         @if ($errors->has('name'))
                          <span class="text-danger">{{ $errors->first('name') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Phone#</label>
                    <input type="number" class="form-control" placeholder="Phone number" name="phone" value="{{old('phone')}}" required>

                     @if ($errors->has('phone'))
                          <span class="text-danger">{{ $errors->first('phone') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Email</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="email" class="form-control" placeholder="Email address" name="email" value="{{old('email')}}" required>

                         @if ($errors->has('email'))
                          <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Relation</label>
                             <input type="text" class="form-control" placeholder="Relation" name="relation" value="{{old('relation')}}" required>

                              @if ($errors->has('relation'))
                          <span class="text-danger">{{ $errors->first('relation') }}</span>
                      @endif
                        </div>
                  </div>
                

                 </div>

                 

                 <div class="row">
            

                   <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Address</label>
                             <textarea class="form-control" rows="3" maxlength="300" placeholder="Write your address here...." name="address" required>{{old('address')}}</textarea>
                              @if ($errors->has('address'))
                          <span class="text-danger">{{ $errors->first('address') }}</span>
                      @endif
                        </div>
                  </div>
                 </div>

                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save Contact</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection