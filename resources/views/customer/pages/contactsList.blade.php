@extends('customer.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contact List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('customer.contact.add')}}" type="button" class="btn btn-warning btn-sm" >Add Contacts</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
            
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Contacts</h3>
              </div>
              <!-- style="@media only screen and (max-width: 1900px) { displays: inline-table;}}" -->
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-responsive" style="display: inline-table;">
                  <thead>                  
                    <tr>
                      <th>Name</th>
                      <th >Phone#</th>
                      <th >Relation</th>
                      <th >Email</th>
                      <th >Address</th>
                      <th >Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if(empty($contacts))

                          <tr>
                          <td></td>
                           <td></td>
                           <td></td>
                            <td>No Data</td>
                            <td></td>
                             <td></td>
                          </tr>
                
                    @else

                             @foreach($contacts as $dt)
                          <tr>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->phone }}</td>
                            <td>{{ $dt->relation }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ $dt->address }}</td>
                            <td>
                               <a href="#"type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#contactEdit{{$dt->id}}" >Edit</a>
                                @include('customer.pages.modals.contactEditModal',['dt'=>$dt])
                            </td>
                       
                          </tr>
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $contacts->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
      
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





  </div>
  <!-- /.content-wrapper -->




@endsection