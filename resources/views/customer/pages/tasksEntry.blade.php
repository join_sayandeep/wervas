@extends('customer.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create a Task</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{route('customer.task.list')}}"type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">New Task</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('customer.task.create') }}" method="POST" enctype="multipart/form-data">
              	{{csrf_field()}}
                <div class="card-body">
                 <div class="row">

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Task Priority</label>
                    <select class="form-control" name="task_priority" required>
                          <option value="Normal">Normal</option>
                          <option value="Urgent">Urgent</option>
                        </select>
                     @if ($errors->has('task_priority'))
                          <span class="text-danger">{{ $errors->first('task_priority') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Customer Email (Primary)</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="email" class="form-control" placeholder="Your email address" name="customer_email" value="{{old('customer_email')}}" required>

                         @if ($errors->has('customer_email'))
                          <span class="text-danger">{{ $errors->first('customer_email') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Task Type</label>
                             <input type="text" class="form-control" placeholder="Type of this task" name="task_type" value="{{old('task_type')}}">

                              @if ($errors->has('task_type'))
                          <span class="text-danger">{{ $errors->first('task_type') }}</span>
                      @endif
                        </div>
                  </div>
                

                 </div>

                 <div class="row">
           

                 	<div class="col-md-6">
                 	    <div class="form-group">
                             <label for="exampleInputPassword1">Subject</label>
                             <input type="text" class="form-control" placeholder="Subject" name="subject" value="{{old('subject')}}" required>

                              @if ($errors->has('subject'))
			                    <span class="text-danger">{{ $errors->first('subject') }}</span>
			                @endif

                        </div>
                 	</div>

                 
                    <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Attachment</label>
                             <input type="file" class="form-control" name="task_attachment[]" value="{{old('task_attachment')}}" accept=".jpg,.png,.pdf,.jpeg" multiple>
                              @if ($errors->has('task_attachment'))
                          <span class="text-danger">{{ $errors->first('task_attachment') }}</span>
                      @endif
                        </div>
                  </div>

                 </div>

                 <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Description</label>
                             <textarea class="form-control" rows="3" placeholder="Write description here...." name="description" required>{{old('description')}}</textarea>
                              @if ($errors->has('description'))
                          <span class="text-danger">{{ $errors->first('description') }}</span>
                      @endif
                        </div>
                  </div>
                  
                   <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Comments</label>
                              <textarea class="form-control" rows="3" maxlength="400" placeholder="Write your comments here...." name="comments" >{{old('comments')}}</textarea>

                              @if ($errors->has('comments'))
                          <span class="text-danger">{{ $errors->first('comments') }}</span>
                      @endif

                        </div>
                  </div>

                 </div>
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create New Task</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection