@extends('customer.layouts.AuthLayout')

@section('content')

 
<div class="register-box">
  <div class="register-logo">
    <a href="#"><b>WERVAS</b>CRM</a>
  </div>
 
  <div class="card">
    @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
          
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form action="{{route('customer.register')}}" method="post">
        {{csrf_field()}}
        <div class="input-group mb-3">
          <select class="form-control" name="salutation"  required>
                          <option>Mr.</option>
                           <option>Ms.</option>
                            <option>Mrs.</option>
                       
                        </select>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('salutation') }}</p>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="First Name" name="fname" value="{{old('fname')}}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('fname') }}</p>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Last Name" name="lname" value="{{old('lname')}}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('lname') }}</p>
            <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('username') }}</p>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Contact Number" name="contact_number" value="{{old('contact_number')}}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('contact_number') }}</p>
         <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" name="email" value="{{old('email')}}" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('email') }}</p>
           <div class="input-group mb-3">
        <textarea class="form-control" rows="3" placeholder="Shipping Address" name="shipping_address" required>
            {{old('shipping_address')}}
          </textarea>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-address-book"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('shipping_address') }}</p>

           <?php $plan_data = App\Services\CustomerServices::getAllPlanPackages(); ?>
           <div class="input-group mb-3">
          <select class="form-control" name="plan_package" required>
                          <option>--- Select a plan ---</option>
                          @foreach($plan_data as $plan)
                          <option value="{{$plan->id}}">{{$plan->plan_name}}</option>
                          @endforeach
                        </select>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-paper-plane"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('plan_package') }}</p>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
             <!--  <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label> -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->



@endsection