@extends('customer.layouts.dashboard')


@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Task Information</h1>
          </div>
          <div class="col-sm-6">
           
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Task # - {{$task_data->task_no}} 
          	<span class="right badge badge-danger">
          	 					@if($task_data->task_status == 1)
                                   Open Task
                                @elseif($task_data->task_status == 2)
                                   In Progress
                                @elseif($task_data->task_status == 3)
                                   Follw Up
                                @elseif($task_data->task_status == 4)
                                    Response Needed
                                @elseif($task_data->task_status == 0)
                                    Closed
                                @else
                                    No Status
                                @endif

          	</span>
          </h3>

          <div class="card-tools">
             <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
           
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            
            <div class="col-6 col-md-6 col-lg-6 order-1 order-md-2">
              <div class="text-muted">
                <p class="text-sm">
                	<b class="d-block">Customer Mail Address</b>

                	{{$task_data->customer_email}}
                  
                </p>

                <p class="text-sm">
                	<b class="d-block">Subject</b>

                	{{$task_data->subject}}
                  
                </p>


                <p class="text-sm"><b class="d-block">Description</b>
                  {{$task_data->description}}
                </p>

                <p class="text-sm"><b class="d-block">Comment</b>
                  {{$task_data->comments}}
                </p>

                <p class="text-sm"><b class="d-block">Resolutions</b>
                  {{$task_data->resolutions}}
                </p>


              </div>

              <h5 class="mt-5 text-muted">Attached files</h5>
              <ul class="list-unstyled">
                
                <li>
                  @foreach($task_attachment_data as $attdata)
                 @if(pathinfo($attdata->task_attachments, PATHINFO_EXTENSION) == 'pdf')
                  
                  
                 <a href="{{ route('attachment.display',$attdata->task_attachments) }}" target="_blank" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i>{{$attdata->task_attachments}}</a>
                 
                 @else

                 
                 <a href="{{ route('attachment.display',$attdata->task_attachments) }}" target="_blank" class="btn-link text-secondary"><i class="far fa-fw fa-image "></i>{{$attdata->task_attachments}}</a>
                 
                 @endif
                 <br>
                 @endforeach

                </li>
              </ul>
              
            </div>

            <div class="col-6 col-md-6 col-lg-6 order-1 order-md-2">
            	<div class="text-muted">
                <p class="text-sm">
                	<b class="d-block">Task Priority</b>

                	{{$task_data->task_priority}}
                  
                </p>

                <p class="text-sm">
                	<b class="d-block">Created By</b>

                	{{$task_data->created_by}}
                  
                </p>


                <p class="text-sm"><b class="d-block">Assigned Associate</b>
                  {{$task_data->assigned_associate}}
                </p>

           
                <p class="text-sm"><b class="d-block">Last Modified</b>
                  {{$task_data->last_modified}}
                </p>

                <p class="text-sm"><b class="d-block">Task Type</b>
                  {{$task_data->task_type}}
                </p>


              </div>

            	<div class="text-center mt-5 mb-3">
                <a href="#" data-toggle="modal" data-target="#commentsontask{{$task_data->id}}" class="btn btn-sm btn-primary">Add Comment</a>
                @include('customer.pages.modals.commentsOnTasksModal',['dt'=>$task_data])
                <a href="{{route('customer.task.list')}}" class="btn btn-sm btn-warning">Back</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

<section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Comment from {{$task_data->task_no}}
          </h3>

          <div class="card-tools">
           
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-8 col-md-8 col-lg-12 order-1 order-md-2">
            	<div class="text-muted">
                <p class="text-sm">
                  
                </p>

              </div>

            	<div class="text-center mt-5 mb-3">
                <a href="#" class="btn btn-sm btn-primary">Add Comment</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      </section>




  </div>
  <!-- /.content-wrapper -->

              



@endsection