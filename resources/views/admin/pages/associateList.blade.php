@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Associates</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('admin.view.associate')}}" type="button" class="btn btn-warning btn-sm" >Add Associates</a>
            </ol>
          </div>
        </div>
             @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
                    
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Associates</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-responsive">
                  <thead>                  
                    <tr>
                      <th style="width: 20px;">Username</th>
                      <th>Name</th>
                      <th>Address</th>
                      <th>Email</th>
                      <th>Contact #</th>
                      <th>Date of Joining</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if(empty($associate_data))

                          <tr>
                          <td></td>
                           <td></td>
                           <td></td>
                            <td>No Data</td>
                            <td></td>
                             <td></td>
                             <td></td>
                          </tr>
                
                    @else

                             @foreach($associate_data as $dt)
                          <tr>
                            <td>{{ $dt->username }}</td>
                            <td>{{ $dt->fname }} {{ $dt->lname }}</td>
                            <td>{{ $dt->address }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ $dt->contact_number }}</td>
                            <td>{{ $dt->doj }}</td>
                            <td>@if($dt->account_status==1)
                                 <a href="#"type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#associatesAccStatus{{$dt->id}}" >Deactivate</a>
                                  @include('admin.pages.modals.associateAccountStatusModal',['id'=>$dt->id,'dt'=>$dt,'status'=>0])
                                @else
                                   <a href="#"type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#associatesAccStatus{{$dt->id}}" >Activate</a>
                                  @include('admin.pages.modals.associateAccountStatusModal',['id'=>$dt->id,'dt'=>$dt,'status'=>1])
                                @endif
                            </td> 
                          </tr>
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $associate_data->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
      
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





  </div>
  <!-- /.content-wrapper -->




@endsection