@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Associate</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{route('admin.associate.list')}}"type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">New Associate</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('admin.associate.save') }}" method="POST">
              	{{csrf_field()}}
                <div class="card-body">
                 <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">First Name</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="text" class="form-control" placeholder="First Name" name="fname" value="{{old('fname')}}" required>
                         @if ($errors->has('fname'))
                          <span class="text-danger">{{ $errors->first('fname') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Last Name</label>
                    <input type="text" class="form-control" placeholder="Last Name" name="lname" value="{{old('lname')}}" required>

                     @if ($errors->has('lname'))
                          <span class="text-danger">{{ $errors->first('lname') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>
                
                
                <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Username</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}" required>

                         @if ($errors->has('username'))
                          <span class="text-danger">{{ $errors->first('username') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Contact #</label>
                             <input type="text" class="form-control" placeholder="Contact Number" name="contact_number" value="{{old('contact_number')}}" required>

                              @if ($errors->has('contact_number'))
                          <span class="text-danger">{{ $errors->first('contact_number') }}</span>
                      @endif
                        </div>
                  </div>
                

                 </div>

                 <div class="row">
           

                 	<div class="col-md-6">
                 	    <div class="form-group">
                             <label for="exampleInputPassword1">Email Id (Primary)</label>
                             <input type="email" class="form-control" placeholder="Email" name="email" value="{{old('email')}}" required>

                              @if ($errors->has('email'))
			                    <span class="text-danger">{{ $errors->first('email') }}</span>
			                @endif

                        </div>
                 	</div>

                    <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Date of Joining</label>
                              <input type="date" class="form-control" placeholder="Date of Joining" name="doj" value="{{old('doj')}}" required>

                              @if ($errors->has('doj'))
                          <span class="text-danger">{{ $errors->first('doj') }}</span>
                      @endif

                        </div>
                  </div>


                 </div>

                 <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Address</label>
                             <textarea class="form-control" rows="3" placeholder="Address" name="address" required>{{old('address')}}</textarea>
                              @if ($errors->has('address'))
                          <span class="text-danger">{{ $errors->first('address') }}</span>
                      @endif
                        </div>
                  </div>

                

                 </div>

                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection