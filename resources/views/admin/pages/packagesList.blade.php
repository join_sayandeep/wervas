@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Plan Packages</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('admin.packages.view')}}" type="button" class="btn btn-warning btn-sm" >Add Packages</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
            
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Packages</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-responsive">
                  <thead>                  
                    <tr>
                      <th style="width: 20px;">Plan Name</th>
                      <th>Description</th>
                      <th>Validity (In Days)</th>
                      <th>Amount ($)</th>
                      <th>Paypal URL</th>
                      <th>Total Point Charged</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if($package_data->isEmpty())

                          <tr>
                          <td></td>
                           <td></td>
                           <td></td>
                            <td>No Data</td>
                            <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                          </tr>
                
                    @else

                             @foreach($package_data as $dt)
                          <tr>
                            <td>{{ $dt->plan_name }}</td>
                            <td>{{ $dt->description }}</td>
                            <td>{{ $dt->validity_in_days }}</td>
                            <td>{{ $dt->package_amount }}</td>
                            <td>{{ $dt->paypal_url }}</td>
                            <td>{{ $dt->total_point_charged }}</td>
                            <td>@if($dt->status==1)
                                  <a href="{{ route('admin.packages.status',[0,$dt->id])}} "type="button" class="btn btn-danger btn-sm" >Deactivate</a>
                                @else
                                        <a href="{{ route('admin.packages.status',[1,$dt->id])}} "type="button" class="btn btn-success btn-sm" >Activate</a>
                                @endif
                            </td>
                            <td>
                               <a href="#"type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#package-edit{{$dt->id}}" >Edit</a>
                                @include('admin.pages.modals.packageInfoEditModal',['id'=>$dt->id,'dt'=>$dt])
                            </td>
                       
                          </tr>
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $package_data->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
      
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





  </div>
  <!-- /.content-wrapper -->




@endsection