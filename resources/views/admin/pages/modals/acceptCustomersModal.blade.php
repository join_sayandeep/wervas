<div class="modal fade" id="acceptCustomers{{$id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Accept New Customer</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{ route('admin.customer.accept') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="uname" value="{{$dt->username}}">
                <input type="hidden" name="email" value="{{$dt->email}}">
               <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputEmail1">Next Payment Date</label>
                    <input type="date" class="form-control" name="next_payment_date" id="exampleInputEmail1" placeholder="Next Payment Date" required>

                        @if ($errors->has('next_payment_date'))
                          <span class="text-danger">{{ $errors->first('next_payment_date') }}</span>
                      @endif

                  </div>
                  </div>
                 </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Save changes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->