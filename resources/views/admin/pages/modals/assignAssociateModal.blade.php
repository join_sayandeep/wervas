<div class="modal fade" id="assignAssociates{{$dt->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Assign to a Associate [Task# {{$dt->task_no}}]</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{ route('admin.assign.associate') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$dt->id}}">
                <input type="hidden" name="task_no" value="{{$dt->task_no}}">
               <div class="row">
                  <div class="col-md-10">
                    <div class="form-group" >
                  @php $assoc_data = App\Http\Controllers\AssociateController::Associates(); @endphp
                         <select class="form-control select2 " style="width: 100%;"  tabindex="-1" name="assigned_associate">
                
                          <option selected="selected" value="">------------------Select an Associate ------------------------</option>
                            @foreach($assoc_data as $data)
                            <option value="{{$data->username}}">{{$data->fname}} {{$data->lname}} [{{$data->username}}]</option>
                            @endforeach
                          </select>
                    </div>
                  </div>
                 </div>

                  <div class="row">
                  <div class="col-md-10">
                    <div class="form-group" >
                     <input type="text" class="form-control" placeholder="Total Points" name="total_point_charged" value="{{old('total_point_charged')}}" required>
                    </div>
                  </div>
                 </div>


            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success">Assign Now</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->