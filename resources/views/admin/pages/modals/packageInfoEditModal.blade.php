<div class="modal fade" id="package-edit{{$id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Edit Package</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{ route('admin.packages.update') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$id}}">
               <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputEmail1">Plan Name</label>
                    <input type="text" class="form-control" name="plan_name" id="exampleInputEmail1" placeholder="Plan Name" value="{{$dt->plan_name}}" required>

                            @if ($errors->has('plan_name'))
                          <span class="text-danger">{{ $errors->first('plan_name') }}</span>
                      @endif

                  </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputFile">Validity (In Days)</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="number" class="form-control" name="validity_in_month" id="exampleInputEmail1" placeholder="Validity (In Month)" value="{{$dt->validity_in_days}}" required>
                         @if ($errors->has('validity_in_month'))
                          <span class="text-danger">{{ $errors->first('validity_in_month') }}</span>
                      @endif
                      </div>
                    </div>
                  </div>
                  </div>

                 </div>

                 <div class="row">
                  <div class="col-md-12">
                         <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea class="form-control" rows="3" name="description" placeholder="Enter ..." required>{{$dt->description}}</textarea>

                     @if ($errors->has('description'))
                          <span class="text-danger">{{ $errors->first('description') }}</span>
                      @endif

                  </div>
                  </div>

                 </div>

                 <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Price</label>
                             <input type="number" class="form-control" name="price" id="exampleInputEmail1" placeholder="Price of Plan" value="{{$dt->package_amount}}" required>
                              @if ($errors->has('price'))
                          <span class="text-danger">{{ $errors->first('price') }}</span>
                      @endif
                        </div>
                  </div>

                  

                 </div>

                  <div class="row">

                  <div class="col-md-12">
                      <div class="form-group">
                             <label for="exampleInputPassword1">Paypal Link (Subscription)</label>
                             <input type="text" class="form-control" name="paypal_link" id="exampleInputEmail1" placeholder="Paypal Link (Subscription)" value="{{$dt->paypal_url}}" required>
                              @if ($errors->has('paypal_link'))
                          <span class="text-danger">{{ $errors->first('paypal_link') }}</span>
                      @endif

                        </div>
                  </div>

                 </div>

                
                  <div class="form-check">
                    @if($dt->status == 0)
                    <input type="checkbox" class="form-check-input" name="status" >
                    <label for="exampleCheck1">Active</label>
                    @else
                    <input type="checkbox" class="form-check-input" name="status" checked>
                    <label for="exampleCheck1">Active</label>
                    @endif

                  </div>
               
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Save changes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->