<div class="modal fade" id="associatesAccStatus{{$id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-warning">
              <h4 class="modal-title">Associate Account Status</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form role="form" action="{{ route('admin.associate.status.update') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$id}}">
                <input type="hidden" name="status" value="{{ $status }}">
               <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                    <label for="exampleInputEmail1">Are your sure?</label>
                  </div>
                  </div>
                 </div>

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-secondary">Save changes</button>
            </div>
            
          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->