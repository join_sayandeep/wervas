@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Plan Packages</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('admin.packages.list')}} "type="button" class="btn btn-warning btn-sm" >View All</a>
            </ol>
          </div>
        </div>

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Packages</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('admin.packages.save') }}" method="POST">
              	{{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 	<div class="col-md-6">
                 		  <div class="form-group">
                    <label for="exampleInputEmail1">Plan Name</label>
                    <input type="text" class="form-control" name="plan_name"  placeholder="Plan Name" value="{{old('plan_name')}}" required>

                            @if ($errors->has('plan_name'))
			                    <span class="text-danger">{{ $errors->first('plan_name') }}</span>
			                @endif

                  </div>
                 	</div>
                 	<div class="col-md-6">
                 		  <div class="form-group">
                    <label for="exampleInputFile">Validity (In Days)</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="number" class="form-control" name="validity_in_month"  placeholder="Validity (In Month)" value="{{old('validity_in_month')}}" required>
                         @if ($errors->has('validity_in_month'))
			                    <span class="text-danger">{{ $errors->first('validity_in_month') }}</span>
			                @endif
                      </div>
                    </div>
                  </div>
                 	</div>

                 </div>
                
                
                <div class="row">
                 	<div class="col-md-12">
                 		     <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea class="form-control" rows="3" name="description" placeholder="Enter ..." required>{{old('description')}}</textarea>

                     @if ($errors->has('description'))
			                    <span class="text-danger">{{ $errors->first('description') }}</span>
			                @endif

                  </div>
                 	</div>

                 </div>

                 <div class="row">
                 	<div class="col-md-6">
                 	    <div class="form-group">
                             <label for="exampleInputPassword1">Price</label>
                             <input type="number" class="form-control" name="price"  placeholder="Price of Plan" value="{{old('price')}}" required>
                              @if ($errors->has('price'))
			                    <span class="text-danger">{{ $errors->first('price') }}</span>
			                @endif
                        </div>
                 	</div>

                 	<div class="col-md-6">
                 	    <div class="form-group">
                             <label for="exampleInputPassword1">Paypal Link (Subscription)</label>
                             <input type="text" class="form-control" name="paypal_link"  placeholder="Paypal Link (Subscription)" value="{{old('paypal_link')}}" required>
                              @if ($errors->has('paypal_link'))
			                    <span class="text-danger">{{ $errors->first('paypal_link') }}</span>
			                @endif

                        </div>
                 	</div>

                 </div>

                 <div class="row">

                  <div class="col-md-6">
                          <div class="form-group">
                             <label for="exampleInputPassword1">Total Point Charged</label>
                             <input type="text" class="form-control" name="total_point_charged"  placeholder="Total Points" value="{{old('total_point_charged')}}" required>
                              @if ($errors->has('total_point_charged'))
                          <span class="text-danger">{{ $errors->first('total_point_charged') }}</span>
                      @endif

                        </div>
                  </div>

                 </div>
                
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="status" >
                    <label for="exampleCheck1">Active The Plan Now</label>
                  </div>




                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



            <!-- Input addon -->
           
            <!-- Horizontal Form -->
           
          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection