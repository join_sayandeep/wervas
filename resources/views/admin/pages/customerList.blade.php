@extends('admin.layouts.dashboard')


@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Clients</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
             <a href="{{ route('admin.customer.create')}}" type="button" class="btn btn-warning btn-sm" >Add Clients</a>
            </ol>
          </div>
        </div>
             @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            
                    
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Clients List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-responsive">
                  <thead>                  
                    <tr>
                      <th style="width: 20px;">Username</th>
                      <th>Name</th>
                      <th>Business Contact#</th>
                      <th>Email</th>
                      <th>Country</th>
                      <th>Last Payment</th>
                      <th>Next Payment</th>
                       <th>Current Plan</th>
                      <th>Shipping Address</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @if(empty($customer_data))

                          <tr>
                          <td></td>
                           <td></td>
                           <td></td>
                            <td>No Data</td>
                            <td></td>
                             <td></td>
                             <td></td>
                          </tr>
                
                    @else

                             @foreach($customer_data as $dt)
                          <tr>
                            <td><a href="{{route('admin.customer.profile',$dt->username)}}">{{ $dt->username }}</a></td>
                            <td><a href="{{route('admin.customer.profile',$dt->username)}}" > {{ $dt->fname }} {{ $dt->lname }}</a></td>
                            <td>{{ $dt->business_phone }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ $dt->country }}</td>
                            <td>{{ $dt->last_payment_date }}</td>
                            <td>{{ $dt->next_payment_date }}</td>

                            @foreach($packages_list as $list)
                               @if($list->id == $dt->current_plan)
                                <td>{{ $list->plan_name }}</td>
                               @endif
                            @endforeach

                            <td>{{ $dt->shipping_address }}</td>
                            <td>@if($dt->account_status==1)
                                  <a href="#" type="button" class="btn btn-warning btn-sm" >Accepted</a>
                                @elseif($dt->account_status==2)
                                  <a href="#" type="button" class="btn btn-danger btn-sm" >Account Freezed</a>
                                @else
                                  <a href="#"type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#acceptCustomers{{$dt->id}}" >Accept</a>
                                  @include('admin.pages.modals.acceptCustomersModal',['id'=>$dt->id,'dt'=>$dt])
                                @endif
                            </td>
                       
                          </tr>
                       @endforeach
                    @endif
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                   {{ $customer_data->render("pagination::bootstrap-4") }}
                </ul>
              </div>
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
        
        </div>
        <!-- /.row -->
      
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->





  </div>
  <!-- /.content-wrapper -->




@endsection