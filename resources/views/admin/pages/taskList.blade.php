@extends('admin.layouts.dashboard')


@section('content')


<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tasks</h1>
          </div>
          <div class="col-sm-6">
             
          </div>
        </div>
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">

          @foreach($task_data as $data)

            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                 Task# - {{$data->task_no}}
                  @if($data->task_status == 1)
                 <span class="badge badge-pill badge-dark">Open Task</span>
                 @elseif($data->task_status == 2)
                 <span class="badge badge-pill badge-success">In Progress</span>
                 @elseif($data->task == 3)
                 <span class="badge badge-pill badge-dark">Follow Up</span>
                 @elseif($data->task == 4)
                 <span class="badge badge-pill badge-warning">Response Needed</span>
                 @elseif($data->task == 5)
                 <span class="badge badge-pill badge-danger">Closed</span>
                 @else
                 <span class="badge badge-pill badge-dark">Assigned</span>
                 @endif
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-10">
                      <h2 class="lead"><b>{{$data->subject}}</b></h2>
                      <p class="text-muted text-sm"><b>Comments:</b>  {{$data->comments}} </p>
                      
                        <p class="text-muted text-sm"><b >Task Priority: </b> {{$data->task_priority}}</p>
                        <p class="text-muted text-sm"><b >Customer Email: </b> {{$data->customer_email}}</p>
                      
                    </div>
                   <!--  <div class="col-5 text-center">
                      <img src="../../dist/img/user1-128x128.jpg" alt="" class="img-circle img-fluid">
                    </div> -->
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">

                  	@if($data->task_status == 999)

                  		<a href="#"  data-toggle="modal" data-target="#assignedAssociates{{$data->id}}" class="btn btn-sm bg-danger">
                           <i class="fa fa-user" aria-hidden="true"></i>
                        </a>

                          @include('common.modals.assignedAssociateMsg',['dt'=>$data])
                    

                  	@else
                    <a href="#" data-toggle="modal" data-target="#assignAssociates{{$data->id}}" class="btn btn-sm bg-teal">
                      <i class="fa fa-user-plus" aria-hidden="true"></i>
                    </a>

                    @include('admin.pages.modals.assignAssociateModal',['dt'=>$data])

                    @endif


                    <a href="{{route('admin.task.info',[$data->id])}}" class="btn btn-sm btn-warning">
                      <i class="fa fa-info-circle" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            
            
           
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
             {{ $task_data->render("pagination::bootstrap-4") }}
          </nav>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection