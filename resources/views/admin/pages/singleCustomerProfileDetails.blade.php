@extends('admin.layouts.dashboard')


@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$single_customer->fname}}'s Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Customer Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-md-1">
        	</div>
          <div class="col-md-10">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                 
                </div>

                <h3 class="profile-username text-center">{{$single_customer->salutation}} {{$single_customer->fname}} {{$single_customer->lname}}</h3>

                <p class="text-muted text-center"><strong>DOB:</strong> {{$single_customer->dob}}</p>
                <p class="text-muted text-center"><strong>Business Number:</strong> {{$single_customer->business_phone}}</p>
                <p class="text-muted text-center"><strong>Shipping Address:</strong> {{$single_customer->shipping_address}}</p>


                <ul class="list-group list-group-unbordered mb-2">
                  <li class="list-group-item">
                    <b>Username</b> <a class="float-right">{{$single_customer->username}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Contact#</b> <a class="float-right">{{$single_customer->contact_number}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email-ID</b> <a class="float-right">{{$single_customer->email}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Secondary Email: </b> <a class="float-right">{{$single_customer->secondary_email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Website</b> <a href="{{$single_customer->website}}" target="_blank" class="float-right">{{$single_customer->website}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Country</b> <a class="float-right">{{$single_customer->country}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Last Payment Date</b> <a class="float-right">{{$single_customer->last_payment_date}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Next Payment Date</b> <a class="float-right">{{$single_customer->next_payment_date}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Current Plan</b> <a class="float-right">

                    	
                            @foreach($packages_list as $list)
                               @if($list->id == $single_customer->current_plan)
                                <td>{{ $list->plan_name }}</td>
                               @endif
                            @endforeach

                    </a>
                  </li>

                  <li class="list-group-item">
                    <b>Street Address</b> <a class="float-right">{{$single_customer->street_address}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Skype ID</b> <a class="float-right">{{$single_customer->skype_id}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>City</b> <a class="float-right">{{$single_customer->city}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>State</b> <a class="float-right">{{$single_customer->state}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Zip</b> <a class="float-right">{{$single_customer->zip}}</a>
                  </li>
                  
                </ul>

                @if($single_customer->account_status==1)
                <a href="{{route('admin.customer.profile.status',[$single_customer->username,2])}}" class="btn btn-danger btn-block"><b>Deactivate Account</b></a>
                @elseif($single_customer->account_status==2)
                <a href="{{route('admin.customer.profile.status',[$single_customer->username,1])}}" class="btn btn-success btn-block"><b>Activate Account</b></a>
                @else
                <a href="#" class="btn btn-secondary btn-block" disabled><b>Deactivate Account</b></a>
                @endif
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

      
          </div>
          <!-- /.col -->
          <div class="col-md-1">
        	</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection