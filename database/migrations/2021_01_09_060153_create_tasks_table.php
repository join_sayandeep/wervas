<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id(); 
            $table->string('task_no',100);
            $table->string('task_type',100)->nullable();
            $table->string('customer_email',100);
            $table->string('subject',100);
            $table->string('description',500);
            $table->string('comments',400)->nullable();
            $table->string('resolutions',300)->nullable();
            $table->string('task_priority',100);
            $table->string('created_by',100);
            $table->string('assigned_associate',100)->nullable();
            $table->string('last_modified',100)->nullable();
            $table->string('task_status',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
