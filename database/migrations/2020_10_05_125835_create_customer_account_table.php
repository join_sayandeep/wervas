<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_account', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',150);
            $table->string('salutation',150);
            $table->string('fname',100);
            $table->string('lname',100);
            $table->string('contact_number',100);
            $table->string('dob',100)->nullable();
            $table->string('business_phone',100)->nullable();
            $table->string('email',100);
            $table->string('secondary_email',100)->nullable();
            $table->string('website',100)->nullable();
            $table->string('country',100)->nullable();
            $table->string('last_payment_date',100)->nullable();
            $table->string('next_payment_date',100)->nullable();
            $table->string('current_plan',100)->nullable();
            $table->string('next_plan',100)->nullable();
            $table->string('street_address',100)->nullable();
            $table->string('skype_id',100)->nullable();
            $table->string('city',100)->nullable();
            $table->string('state',100)->nullable();
            $table->string('zip',100)->nullable();
            $table->string('shipping_address',200)->nullable();
            $table->integer('account_status')->length(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_account');
    }
}
