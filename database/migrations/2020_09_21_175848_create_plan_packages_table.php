<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('plan_name',150);
            $table->string('description',400);
            $table->string('validity_in_days',50);
            $table->string('paypal_url',500);
            $table->integer('package_amount')->length(100);
            $table->stirng('total_point_charged',100)->nullable();
            $table->integer('status')->length(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_packages');
    }
}
