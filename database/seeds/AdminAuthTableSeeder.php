<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminAuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\AdminRegister::create([
            'name' => 'WERVAS',
            'email' => 'wervas@gmail.com',
            'phone_no' => 1234569875,
            'username' => 'wervas_admin'
        ]);

        App\AdminLogin::create([
            'username' => 'wervas_admin',
            'password' => Hash::make('admin123')
        ]);
    }
}
